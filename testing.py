import pandas as pd
import json
import utility as util
def get_filtered_data(dataset:str):
    '''
    Filter the dataset and returns a Pandas Dataframe of it.
    '''
    df = pd.read_json(dataset)

    # df.dropna(inplace=True)
    
    # Filter out data
    # Remove mods with status of not published
    df_filter = df[df['status'].str.contains('not_published')]
    df.drop(df_filter.index, inplace=True)

    # Remove mods that do not allow user rating (endorsements)
    df_filter = df[df['allow_rating'] == False]
    df.drop(df_filter.index, inplace=True)

    # Remove mods that are younger than 6 months old
    df_filter = df[df['created_time'] > '2021-04-29']
    df.drop(df_filter.index, inplace=True)

    # Drop rows with no endorsement data
    df.dropna(axis=0, subset=['endorsement'], inplace=True)

    # Convert NaN into empty string
    df['name'].fillna('', inplace=True)
    df['summary'].fillna('', inplace=True)
    df['description'].fillna('', inplace=True)
    df['author'].fillna('', inplace=True)
    df['uploaded_by'].fillna('', inplace=True)

    # If contains_adult_content is NaN, change it to False
    df['contains_adult_content'].fillna(False, inplace=True)

    # If category_id is NaN, change it to 28 (miscellaneous)
    df['category_id'].fillna(28, inplace=True)
    
    # Drop unused columns for training
    df.drop(labels=[
        'user', 
        'endorsement',
        'status',
        'allow_rating',
        'description',
        'picture_url',
        'uid',
        'mod_id',
        'game_id',
        'domain_name',          # We just want to focus on skyrimspecialedition domain, so drop it
        'version',
        'created_timestamp',
        'updated_timestamp',
        'updated_time',
        'uploaded_users_profile_url',
        'available',
    ], inplace=True, axis=1)

    # Remove date data from created_time
    df = df.astype({'created_time' : str})
    df['created_time'] = df['created_time'].apply(util.remove_date_from_datetime_string)

    return df


def main():
    df = get_filtered_data('mod_data_prime.json')
    print(df.head(100)['name'])
    df.to_csv('mod_data_desc.csv')
if __name__ == '__main__':
    main()
