import numpy as np
import os
import tensorflow as tf
import pandas as pd
import matplotlib.pyplot as plt
import utility as util
import tensorflow_addons as tfa

from sklearn.metrics import r2_score

def graph(history):
    '''
    Function to plot training graph
    '''
    def plot_graphs(history, metric):
        y = history.history[metric]
        y_val = history.history['val_'+metric]
        x = [i for i in range(len(y))]
        
        # m, b = np.polyfit(x, y, 1)
        # m_val, b_val = np.polyfit(x, y_val, 1)

        plt.plot(y)
        plt.plot(x, np.poly1d(np.poly1d(np.polyfit(x, y, 1))(x)), '--')
        plt.plot(y_val, '')
        plt.plot(x, np.poly1d(np.poly1d(np.polyfit(x, y_val, 1))(x)), '--')
        plt.xlabel("Epochs")
        plt.ylabel(metric)
        plt.legend([metric, f'{metric} (best-fit)', 'val_'+metric, f'val_{metric} (best-fit)'])

    plt.figure(figsize=(32, 16))
    plt.subplot(2, 2, 1)
    plot_graphs(history, 'accuracy')
    plt.ylim(None, 1)
    plt.subplot(2, 2, 2)
    plot_graphs(history, 'loss')
    plt.ylim(0, None)
    plt.subplot(2, 2, 3)
    plot_graphs(history, 'mae')
    plt.subplot(2, 2, 4)
    plot_graphs(history, 'mse')

def load_json_dataset(json_file:str):
    return pd.read_json(json_file)

def train_desc_dataset(
    batch_size: int = 16,
    seed:int = 12,
):
    '''
    Train the RNN model for Description and endorsement
    '''
    # Load and process dataset
    df = pd.read_json('mod_data_desc_end_only.json')
    df['description'].fillna(' ', inplace=True)
    endorsements = df['endorsement_count']

    train_ds, val_ds, test_ds = util.partition_rnn_dataset(df, 'description', batch_size, train_split=0.7, val_split=0.2, test_split=0.1, seed=seed)

    # Create text encoder and normalizer
    encoder = create_encoder(train_ds, max_tokens=10000)
    normalizer = create_normalization_layer(endorsements)

    # Get model
    model = create_base_rnn_model(encoder, normalizer)
    model.compile(          # Compile model
        loss = tf.keras.losses.MeanAbsoluteError(),     
        optimizer = tf.keras.optimizers.Adam(1e-4),
        metrics = ['accuracy', 'mae', 'mse']
    )

    # Train model
    history = train(model, train_ds, val_ds, model_name='desc_model', use_model_checkpoint=True)

    test_loss, test_acc, test_mae, test_mse = model.evaluate(test_ds)

    print('Test Loss:', test_loss)
    print('Test Accuracy:', test_acc)
    print('Test MAE:', test_mae)
    print('Test MSE:', test_mse)

    sample_text = ('Wearable Lanterns')
    print(model.predict(np.array([sample_text])))

    graph(history)
    plt.show()

    return model

def create_base_rnn_model(encoder: tf.keras.layers.TextVectorization, normalized_layer:tf.keras.layers.Normalization):
    return tf.keras.Sequential([
        encoder,
        tf.keras.layers.Embedding(
        input_dim=len(encoder.get_vocabulary()),
        output_dim=64,
        # Use masking to handle the variable sequence lengths
        mask_zero=True),
        normalized_layer,
        tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(64,  return_sequences=True)),
        tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(32)),
        # tf.keras.layers.Dense(64, activation='relu'),
        # tf.keras.layers.Dense(128, activation='relu'),
        # tf.keras.layers.Dense(128, activation='relu'),
        tf.keras.layers.Dense(64, activation='relu'),
        tf.keras.layers.Dense(32, activation='relu'),
        tf.keras.layers.Dense(16, activation='relu'),
        tf.keras.layers.Dropout(0.5),
        tf.keras.layers.Dense(1)
    ])

def create_summary_rnn_model(encoder: tf.keras.layers.TextVectorization, normalized_layer:tf.keras.layers.Normalization):

    return tf.keras.Sequential([
        encoder,
        tf.keras.layers.Embedding(
        input_dim=len(encoder.get_vocabulary()),
        output_dim=64,
        # Use masking to handle the variable sequence lengths
        mask_zero=True),
        normalized_layer,
        # tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(256,  return_sequences=True)),
        # tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(128)),
        tf.keras.layers.GRU(256, return_sequences=True),
        tf.keras.layers.GRU(128),
        tf.keras.layers.Dropout(0.25),
        tf.keras.layers.Dense(64, activation='relu'),
        tf.keras.layers.Dense(32, activation='relu'),
        tf.keras.layers.Dense(16, activation='relu'),
        tf.keras.layers.Dropout(0.5),
        tf.keras.layers.Dense(1)
    ])

def create_name_rnn_model(encoder: tf.keras.layers.TextVectorization, normalized_layer:tf.keras.layers.Normalization):

    return tf.keras.Sequential([
        encoder,
        tf.keras.layers.Embedding(
        input_dim=len(encoder.get_vocabulary()),
        output_dim=64,
        # Use masking to handle the variable sequence lengths
        mask_zero=True),
        # normalized_layer,
        # tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(256,  return_sequences=True)),
        # tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(128)),
        # tf.keras.layers.GRU(256, return_sequences=True),
        tf.keras.layers.GRU(4096),
        tf.keras.layers.Dropout(0.25),
        tf.keras.layers.Dense(256, activation='relu'),
        # tf.keras.layers.Dense(32, activation='relu'),
        # tf.keras.layers.Dense(16, activation='relu'),
        tf.keras.layers.Dropout(0.5),
        tf.keras.layers.Dense(1, activation='relu')
    ])

def create_summary_rnn_model(encoder: tf.keras.layers.TextVectorization, normalized_layer:tf.keras.layers.Normalization):

    return tf.keras.Sequential([
        encoder,
        tf.keras.layers.Embedding(
        input_dim=len(encoder.get_vocabulary()),
        output_dim=64,
        # Use masking to handle the variable sequence lengths
        mask_zero=True),
        # normalized_layer,
        tf.keras.layers.Bidirectional(tf.keras.layers.GRU(4096)),
        tf.keras.layers.Dropout(0.25),
        tf.keras.layers.Dense(256, activation='relu'),
        tf.keras.layers.Dropout(0.5),
        tf.keras.layers.Dense(1, activation='relu')
    ])

def create_encoder(train_ds, max_tokens = 10000):
    encoder = tf.keras.layers.TextVectorization(max_tokens = max_tokens)
    encoder.adapt(train_ds.map(lambda text, label: text))
    return encoder

def create_normalization_layer(label_df):
    normalizer = tf.keras.layers.Normalization(input_shape=[1,], axis=None)
    normalizer.adapt(label_df)
    return normalizer

def train(
    model, 
    train_ds, 
    val_ds, 
    model_name:str = 'model',
    use_early_stopping:bool = True,
    use_tensorboard_callback:bool = True,
    tensorboard_path:str = './tensorboard_logs/',
    use_model_checkpoint:bool = False,
    use_csv_logger:bool = True,
    csv_path:str = './csv_logs/',
    checkpoint_freq:int = 1,
    checkpoint_path:str = './model_checkpoints/',
    epochs:int = 10, 
    verbose:int = 2,
    append_csv:bool= False
):

    # Create callbacks
    callbacks:list = []

    # Tensorboard
    if (use_tensorboard_callback):
        log_dir = tensorboard_path + model_name
        tensorboard_callback = tf.keras.callbacks.TensorBoard(
            log_dir=log_dir, 
            histogram_freq=1,
            embeddings_freq=1,
            write_images=False,
        )
        callbacks.append(tensorboard_callback)
    # Early stopping
    if (use_early_stopping):
        early_stopping_callback = tf.keras.callbacks.EarlyStopping(
            # Stop training when `val_loss` is no longer improving
            monitor="val_loss",
            # "no longer improving" being defined as "no better than 1e-2 less"
            min_delta=1e-2,
            # "no longer improving" being further defined as "for at least 5 epochs"
            patience=5,
            verbose=1
        )
        callbacks.append(early_stopping_callback)
    # Model checkpoints
    if (use_model_checkpoint):
        model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
            # Path where to save the model
            # The two parameters below mean that we will overwrite
            # the current checkpoint if and only if
            # the `loss` score has improved.
            # The saved model name will include the current epoch.
            filepath = checkpoint_path + model_name + '/',
            save_best_only=True,  # Only save a model if `val_loss` has improved.
            monitor="loss", 
            verbose=0,
            save_weights_only=True,
            save_freq = checkpoint_freq
        )
        callbacks.append(model_checkpoint_callback)
    # CSV Logger
    if (use_csv_logger):
        csv_logger = tf.keras.callbacks.CSVLogger(
            csv_path + model_name + '.csv',
            append=append_csv
        )
        callbacks.append(csv_logger)
    
    # Train the model
    print('Begin training model...')
    return model.fit(
        train_ds,
        epochs = epochs,
        validation_data = val_ds,
        callbacks = callbacks,
        verbose = verbose,
    )

def run_training_regiment(
    df:pd.DataFrame,
    feature_key:str,
    model_name:str,
    model_creation_func,
    weight_path:str = '',
    append_csv = True,
    enable_shuffle = True,
    batch_size: int = 32,
    shuffle_size:int = 30000,    # Must be equal or greater than total dataset to achieve full shuffling
    seed:int = 69420,
    train_split= 0.7,
    val_split= 0.2,
    test_split= 0.1,
    max_tokens = 384,
    epochs = 50,
    use_tensorboard_callback = False,
    enable_training:bool = True,
    csv_path:str = './csv_logs/',
    tensorboard_path:str = './tensorboard_logs/',
    checkpoint_path:str = './model_checkpoints/',
):
    # Prepare dataset
    endorsements = df['endorsement_count']
    train_ds, val_ds, test_ds = util.partition_rnn_dataset(
        df, 
        feature_key, 
        batch_size, 
        train_split=train_split,
        val_split=val_split, 
        test_split=test_split, 
        seed=seed,
        shuffle=enable_shuffle,
        shuffle_size=shuffle_size,
    )

    # Create text encoder and normalizer
    encoder = create_encoder(train_ds, max_tokens=max_tokens)
    normalizer = create_normalization_layer(endorsements)

    # Check encoder value
    print('Top 50 encoded words:')
    print(np.array(encoder.get_vocabulary())[:50])

    # Get model
    print()
    model = model_creation_func(encoder, normalizer)
    if (append_csv and weight_path != ''):
        model.load_weights(weight_path)
    model.compile(          # Compile model
        loss = tf.keras.losses.MeanAbsoluteError(),     
        optimizer = tf.keras.optimizers.Adam(1e-4),
        metrics = ['accuracy', 'mae', 'mse']
    )

    # Train model
    history = train(
        model, 
        train_ds, 
        val_ds, 
        use_tensorboard_callback=use_tensorboard_callback,
        model_name=model_name, 
        use_model_checkpoint=True,
        verbose=1,
        epochs=epochs,
        append_csv=append_csv,
        csv_path = csv_path,
        tensorboard_path = tensorboard_path,
        checkpoint_path = checkpoint_path,
    ) if enable_training else None

    return train_ds, val_ds, test_ds, model, history

def graph_training(
    history,
    graph_save_location
):
    # Graph training
    graph(history)
    plt.savefig(graph_save_location)
    plt.show()

def evaluate_model(model, test_ds):
    print('Evaluate Model')
    test_loss, test_acc, test_mae, test_mse = model.evaluate(test_ds)

    # Calculate RMSE
    test_rmse = test_mse**0.5

    # Calculate R^2
    y_true = np.concatenate([y for x, y in test_ds], axis=0)
    y_pred = model.predict(test_ds).ravel()
    test_r2 = r2_score(y_true, y_pred)
    test_r = test_r2**0.5

    # Calculate NRMSE
    y_true_mean = sum(y_true) / len(y_true)
    y_max = max(y_true)
    y_min = min(y_true)
    test_nrmse_mean = test_rmse / y_true_mean
    test_nrmse_minmax = test_rmse / (y_max - y_min)

    print('Test Loss:', test_loss)
    print('Test Accuracy:', test_acc)
    print('Test MAE:', test_mae)
    print('Test MSE:', test_mse)
    print('Test RMSE:', test_rmse)
    print('Test R^2:', test_r2)
    print('Test R:', test_r)
    print('Test NRMSE (Mean):', test_nrmse_mean)
    print('Test NRMSE (Max - Min):', test_nrmse_minmax)

    return y_true, y_pred

def plot_prediction(y_true, y_pred):
    plt.plot(y_true, y_pred, 'o')
    plt.plot(y_true, np.poly1d(np.poly1d(np.polyfit(y_true, y_pred, 1))(y_true)), '--')
    plt.xlabel("Actual Endorsements")
    plt.ylabel("Predicted Endorsements")
    # plt.legend(['Actual Value', 'Predicted Value'])

    plt.show()

def make_prediction(model, test_predict_text):
    sample_text = (test_predict_text)
    print(f'Test prediction for \"{test_predict_text}\"')
    print(model.predict(np.array([sample_text])))

def main():

    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '0' 

    ## Hyperparameteres, change these
    # df = pd.read_json('mod_data_desc_end_only.json')
    df = pd.read_csv('mod_data_name_end_only.csv')
    feature_key:str = 'name'

    # Remove mods more than 1k endorsements
    df.drop(df[df['endorsement_count'] > 1000].index, inplace=True)
    # Set NaN values as empty string
    df[feature_key].fillna(' ', inplace=True)

    model_name = 'name_gru_model_4096'
    model_creation_func = create_name_rnn_model
    graph_save_location = f'./model_checkpoints/{model_name}/{model_name}.png'
    csv_path:str = f'./model_checkpoints/{model_name}/'
    tensorboard_path:str = './model_checkpoints/tensorboard_logs/'
    checkpoint_path:str = './model_checkpoints/'
    enable_training:bool = False
    append_csv = True
    enable_shuffle = True
    batch_size: int = 32
    shuffle_size:int = 30000    # Must be equal or greater than total dataset to achieve full shuffling
    seed:int = 69420
    train_split= 0.7
    val_split= 0.2
    test_split= 0.1
    max_tokens = 384
    epochs = 50
    test_predict_text = 'Black and red leather bikini armor. Modular conversion with top, bottom, boots, gloves and wings pieces to choose from. For CBBE-SE with Bodyslide.'     # Used to test singular input in the model
    weight_path = f'model_checkpoints/{model_name}/'        # Path to previous trained weights. Leave as empty string to not use this feature
    # weight_path = ''

    train_ds, val_ds, test_ds, model, history = run_training_regiment(
        df,
        feature_key = feature_key,
        model_name = model_name,
        model_creation_func = model_creation_func,
        append_csv = append_csv,
        enable_shuffle = enable_shuffle,
        batch_size = batch_size,
        shuffle_size = shuffle_size,
        seed = seed,
        train_split= train_split,
        val_split= val_split,
        test_split= test_split,
        max_tokens = max_tokens,
        epochs = epochs,
        csv_path = csv_path,
        tensorboard_path = tensorboard_path,
        checkpoint_path = checkpoint_path,
        weight_path=weight_path,
        enable_training=enable_training
    )

    # Evaluate model
    print()
    y_true,y_pred = evaluate_model(model, test_ds)
    plot_prediction(y_true, y_pred)

    # Test custom input
    print()
    make_prediction(model, test_predict_text)

    # Graph training
    if (history != None):
        graph_training(history, graph_save_location)

if __name__ == '__main__':
    main()