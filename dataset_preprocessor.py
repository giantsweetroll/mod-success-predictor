import pandas as pd
import dataset_collector as dc
import json
import utility as util

def merge_dataset(main_dataset:str, sub_dataset:str):
    ''' 
    Merges the sub_dataset into the main_dataset.
    Both files must be a Pandas Dataframe compatible JSON format.
    '''
    with open(main_dataset, 'r') as openfile:
        json_data = json.load(openfile)
    with open(sub_dataset, 'r') as openfile:
        json_data2 = json.load(openfile)
    
    dc.merge(json_data, json_data2)

    with open(main_dataset, 'w') as outfile:
        json.dump(json_data, outfile)

def get_filtered_data(dataset_file:str):
    '''
    Filter the dataset and returns a Pandas Dataframe of it.
    '''
    df = pd.read_json(dataset_file)
    print(df)

    # df.dropna(inplace=True)
    
    # Filter out data
    # Remove mods with status of not published
    df_filter = df[df['status'].str.contains('not_published')]
    df.drop(df_filter.index, inplace=True)

    # Remove mods that do not allow user rating (endorsements)
    df_filter = df[df['allow_rating'] == False]
    df.drop(df_filter.index, inplace=True)

    # Remove mods that are younger than 6 months old
    df_filter = df[df['created_time'] > '2021-04-29']
    df.drop(df_filter.index, inplace=True)

    # Drop rows with no endorsement count or name data
    df.dropna(axis=0, subset=['endorsement_count', 'name'], inplace=True)

    # Convert NaN into empty string
    df['name'].fillna(' ', inplace=True)
    df['summary'].fillna(' ', inplace=True)
    df['description'].fillna(' ', inplace=True)
    df['author'].fillna(' ', inplace=True)
    df['uploaded_by'].fillna(' ', inplace=True)

    # Drop empty string
    df.drop(df[(df['name'] == '') | (df['name'] == ' ')].index, inplace=True)
    df.drop(df[(df['summary'] == '') | (df['summary'] == ' ')].index, inplace=True)
    df.drop(df[(df['description'] == '') | (df['description'] == ' ')].index, inplace=True)

    # If contains_adult_content is NaN, change it to False,
    # and then convert the type from boolean to int
    df['contains_adult_content'].fillna(False, inplace=True)
    df = df.astype({'contains_adult_content' : int})

    # If category_id is NaN, change it to 28 (miscellaneous)
    df['category_id'].fillna(28, inplace=True)
    
    # Drop unused columns for training
    df.drop(labels=[
        'user', 
        'endorsement',
        'status',
        'allow_rating',
        'picture_url',
        'uid',
        'mod_id',
        'game_id',
        'domain_name',          # We just want to focus on skyrimspecialedition domain, so drop it
        'version',
        'created_timestamp',
        'updated_timestamp',
        'updated_time',
        'uploaded_users_profile_url',
        'available',
    ], inplace=True, axis=1)

    # Remove date data from created_time
    df = df.astype({'created_time' : str})
    df['created_time'] = df['created_time'].apply(util.remove_date_from_datetime_string)

    return df

def get_df_name_endorsement(main_df:pd.DataFrame):
    '''
    Returns a dataframe that only includes the name of the mod and the amount of
    endrosements
    '''
    return main_df[['name', 'endorsement_count']]

def get_df_summary_endorsement(main_df:pd.DataFrame):
    '''
    Returns a dataframe that only includes the summary of the mod and the amount of
    endrosements
    '''
    return main_df[['summary', 'endorsement_count']]

def get_df_desc_endorsement(main_df:pd.DataFrame):
    '''
    Returns a dataframe that only includes the description of the mod and the amount of
    endrosements
    '''
    return main_df[['description', 'endorsement_count']]

def get_df_tabular(main_df:pd.DataFrame):
    '''
    Trims the dataset to be used for decision tree
    '''
    return main_df.drop(labels=[
        'name',
        'summary',
        'description'
    ], axis=1)

def append_endorsement_classification(df, threshold):
    '''
    Appends a column into the dataframe to identify if a mod has a high or low endorsement count.
    This is determined by the specified threshold. If the mod's endorsement count is < threshold, it is considered
    as having a low endorsement count. If it has >= threshold, it considered to have a high endorsement count.
    '''
    df['high'] = df['endorsement_count'].apply(lambda x: 1 if x >= threshold else 0)
    

def main():
    df = get_filtered_data('mod_data_prime.json')
    threshold = 100
    
    # Name
    name = get_df_name_endorsement(df)
    append_endorsement_classification(name, threshold)
    util.export_csv(name, 'mod_data_name_end_only.csv')

    # Summary
    summary = get_df_summary_endorsement(df)
    append_endorsement_classification(summary, threshold)
    util.export_csv(summary, 'mod_data_summary_end_only.csv')

    # Tabular
    tabular = get_df_tabular(df)
    append_endorsement_classification(tabular, threshold)
    util.export_csv(tabular, 'mod_data_for_decision_tree.csv')

    # Description
    desc = get_df_desc_endorsement(df)
    append_endorsement_classification(desc, threshold)
    util.export_json(desc, 'mod_data_desc_end_only.json')

def perform_undersampling(df):
    '''
    Performs undersampling on dataframe to make classification distribution equal. Returns a new dataframe.
    '''
    df_high = df[df['high'] == 1]
    df_low = df[df['high'] == 0]
    df_low_downsampled = df_low.sample(df_high.shape[0])
    df_balanced = pd.concat([df_high, df_low_downsampled])

    return df_balanced

def perform_oversampling(df):
    '''
    Performs oversampling on dataframe to make classification distribution equal. Returns a new dataframe.
    '''
    df_high = df[df['high'] == 1]
    df_low = df[df['high'] == 0]
    df_high_oversampled = df_high.sample(df_low.shape[0], replace=True)
    return pd.concat([df_low, df_high_oversampled], axis=0)

if __name__ == '__main__':
    main()