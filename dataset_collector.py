import requests
import json
import os

def make_request(api_key:str, game_domain_name:str, mod_id:int):
    ''' Make request to nexus mods api '''
    headers = {
        'apikey' : api_key,
        'accept' : 'application/json'
    }

    response = requests.get(
        url = f'https://api.nexusmods.com/v1/games/{game_domain_name}/mods/{mod_id}.json',
        headers = headers
    )

    return response

def save_to_json(json_filename:str, data:dict):
    ''' Append the new mod data dictionary into json file, formatted for pandas DataFrame '''
    with open(json_filename, 'r') as openfile:
        json_data = json.load(openfile)
        
        json_id = data['mod_id']
        game_name = data['domain_name']

        if 'mod_id' in json_data.keys() and 'domain_name' in json_data.keys():
            if (str(json_id) in json_data['mod_id'].keys()) and (json_data['domain_name'][str(json_id)] != game_name):
                while str(json_id) in json_data['mod_id'].keys():
                    json_id += 3        # Counter duplicate keys
        
        # Append new information
        for key in data.keys():
            if key not in json_data.keys():
                json_data[key] = {}
            if str(json_id) not in json_data[key].keys():
                json_data[key][json_id] = data[key]
    
    with open(json_filename, 'w') as outfile:
        json.dump(json_data, outfile)

def merge(data1:dict, data2:dict):
    ''' 
    Merge two DataFrame dictionaries together. If there are any key conflicts, the conflicting key will be changed.
    Information from data2 will be added/merged to data1.
    '''
    for mod_id in data2['mod_id'].keys():
        # Get game name associated with the mod_id
        game_name = data2['domain_name'][mod_id]
        new_key = mod_id
        
        # Check for conflict in data1
        if (mod_id in data1['mod_id'].keys()) and (data1['domain_name'][mod_id] != game_name):
            while new_key in data1['mod_id'].keys():
                new_key = f'{int(new_key) + 3}'    # assign a new key

        # Copy data to data1
        for category in data2.keys():
            if mod_id in data2[category].keys():
                data1[category][new_key] = data2[category][mod_id]


def gather_data(mod_id_min:int, mod_id_max:int, api_key:str, game_domain_name:str, json_filename:str = 'mod_data.json'):
    ''' 
    Perform data collection process.

    Parameters:
    mod_id_min: get data for mod from this ID
    mod_id_max: get data for mod up to this ID (inclusive)
    '''
    # If file does not exist, create it
    if (not os.path.isfile(f'./{json_filename}')):
        file = open(json_filename, 'w+')
        file.write('{}')
        file.close()

    for i in range(mod_id_min, mod_id_max+1):
        mod_id = i
        print(f'Performing request for mod {mod_id} in game {game_domain_name}...')
        r = make_request(api_key, game_domain_name, mod_id)
        if r.status_code == 200:
            save_to_json(json_filename, r.json())
        elif r.status_code == 429:
            print('API rate limit reached')
            break
        else:
            print(f'Unable to get mod {mod_id} from {game_domain_name}:', r.status_code)

def main():
    api_key:str = 'V0dxZFNQWWtieTdHQVZNMGNtRVlJL3JyNHRiRjQvTThEU2l5SDJHangxb3NQYzlYcUwvYXhlVWozVEpPcEs0di0tR2xscTd1TVlsWTFORUsyVGRxdGxwdz09--6429846287c6d8c12bf83fc622eadc5eb2a70364'
    game_domain_name:str = 'skyrimspecialedition'
    json_filename:str = 'mod_data2.json'

    gather_data(57191, 60000, api_key, game_domain_name, json_filename)

if __name__ == '__main__':
    main()