import pandas as pd
import tensorflow as tf
import numpy as np

def remove_date_from_datetime_string(datetime_str:str):
    return datetime_str[11 : len(datetime_str)]

def export_csv(df:pd.DataFrame, file_name:str):
    df.to_csv(file_name, index=False, encoding='utf-8')

def export_json(df:pd.DataFrame, file_name:str):
    df.to_json(file_name)

def get_dataset_partitions(ds, train_split=0.8, val_split=0.1, test_split=0.1, shuffle=True, shuffle_size=10000, seed=12):
    '''
    Splits the tf.Dataset into training, validation, and test datasets.

    Adapted from:
    https://towardsdatascience.com/how-to-split-a-tensorflow-dataset-into-train-validation-and-test-sets-526c8dd29438
    '''
    assert (train_split + test_split + val_split) == 1
    
    if shuffle:
        # Specify seed to always have the same split distribution between runs
        ds = ds.shuffle(shuffle_size, seed=seed)

    ds_size = len(list(ds))

    train_size = int(train_split * ds_size)
    val_size = int(val_split * ds_size)
    
    train_ds = ds.take(train_size)    
    val_ds = ds.skip(train_size).take(val_size)
    test_ds = ds.skip(train_size).skip(val_size)
    
    return train_ds, val_ds, test_ds

def get_dataframe_partitions(df, train_split = .8, val_split=.1, test_split=.1, seed=12):
  """
  Splits a panda dataframe in to training, validation and test sets.
  
  Adapted from:
  https://stackoverflow.com/questions/38250710/how-to-split-data-into-3-sets-train-validation-and-test
  """
  assert (train_split + test_split + val_split) == 1

  train, validate, test = np.split(df.sample(frac=1, random_state=seed), [int(train_split * len(df)), int((train_split + val_split)*len(df))])
  return train, validate, test

def partition_rnn_dataset(
    df, 
    feature_key,
     batch_size, 
     train_split=0.8, 
     val_split=0.1, 
     test_split=0.1, 
     shuffle=True, 
     shuffle_size=10000, 
     seed=12
):
  '''
  Function to parition the datasets to be fed to RNN model
  '''
  endorsements = df.pop('endorsement_count')
  feature = df[feature_key]
  tf.convert_to_tensor(feature)

  main_ds = tf.data.Dataset.from_tensor_slices((feature, endorsements))

  train_ds, val_ds, test_ds = get_dataset_partitions(
    main_ds, 
    seed=seed,
    train_split=train_split, 
    val_split=val_split, 
    test_split=test_split, 
    shuffle=shuffle, 
    shuffle_size=shuffle_size, 
  )       # Split into train, val, and test datasets

  if (shuffle):
      train_ds = train_ds.shuffle(shuffle_size, seed=seed)

  train_ds = train_ds.batch(batch_size).prefetch(tf.data.AUTOTUNE)
  val_ds = val_ds.batch(batch_size).prefetch(tf.data.AUTOTUNE)
  test_ds = test_ds.batch(batch_size).prefetch(tf.data.AUTOTUNE)
  return train_ds, val_ds, test_ds

def main():
    print(remove_date_from_datetime_string('2018-05-08 16:09:52+00:00'))

if __name__ == '__main__':
    main()